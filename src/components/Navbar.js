import React, {useContext} from 'react';
import {Link, withRouter} from 'react-router-dom';
import AuthContext from '../context/auth/authContext';

const Navbar = props => {
	const {location} = props;

	const homeClass = location.pathname === "/" ? "active" : "";
	const aboutClass = location.pathname.match(/^\/about/) ? "active" : "";
	const galleryClass = location.pathname.match(/^\/gallery/) ? "active" : "";
	const servicesClass = location.pathname.match(/^\/services/) ? "active" : "";
	const bookingClass = location.pathname.match(/^\/booking/) ? "active" : "";
    const adminClass = location.pathname.match(/^\/admin/) ? "active" : "";
    

    const authContext = useContext(AuthContext);
    const { logoutUser, isAuthenticated, user } = authContext;

    const onLogoutClick = e => {
    e.preventDefault();
    logoutUser();
    };

	return (

	<>

	<div id="preloader">
        <div className="loader"></div>
    </div>
    

    
    <div className="top-search-area">
        <div className="modal fade" id="searchModal" tabIndex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-body">
                        
                        <button type="button" className="btn close-btn" data-dismiss="modal"><i className="ti-close"></i></button>
                        
                        <form action="index.html" method="post">
                            <input type="search" name="top-search-bar" className="form-control" placeholder="Search and hit enter..." />
                            <button type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <header className="header-area">
        
        <div className="main-header-area">
            <div className="classy-nav-container breakpoint-off">
                <div className="container">
                    
                    <nav className="classy-navbar justify-content-between" id="alimeNav">

                        
                        <Link to="/" className="nav-brand"><img src="./img/core-img/logo.png" /></Link>

                        
                        <div className="classy-navbar-toggler">
                            <span className="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        
                        <div className="classy-menu">
                            
                            <div className="classycloseIcon">
                                <div className="cross-wrap"><span className="top"></span><span className="bottom"></span></div>
                            </div>
                            
                            <div className="classynav">
                                <ul id="nav">

                                {isAuthenticated && user.email === 'myriad.x.69@gmail.com' ? (<>


                                    <li className={adminClass}><Link to="/admin">Booking List</Link></li>
                                    

                                </>) : (<>

                                    <li className={homeClass}><Link to="/">Home</Link></li>
                                    <li className={aboutClass}><Link to="/about">About</Link></li>
                                    <li className={galleryClass}><Link to="/gallery">Gallery</Link></li>
                                    <li className={servicesClass}><Link to="/services">Services</Link></li>
                                    <li className={bookingClass}><Link to="/booking">Book</Link></li>

                                </>) }
                                    

                                    <li><a href="#">{isAuthenticated ? user.firstname : "User"}</a>
                                        <ul className="dropdown">
                                        {isAuthenticated ? (<li><a href="#logout" onClick={onLogoutClick}>- Logout</a></li>) : (<> <li><Link to ="/login">- Login</Link></li> <li><Link to="/register">- Register</Link></li> </>)}
                                            
                                        </ul>
                                    </li>
                                </ul>

                              
                               
                            </div>
                            
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    </>
	);
};

export default withRouter(Navbar);
