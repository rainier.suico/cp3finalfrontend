import { useEffect } from 'react';

const useScript = url => {


  useEffect(() => {
  	// setTimeout(() => {
	  		const script = document.createElement('script');

	    	script.src = url;
	    	script.async = true;

	    	document.body.appendChild(script);

	    	return () => {
	      	document.body.removeChild(script);
    	}
    // }, 3000);
   
  }, [url]);
};

export default useScript;